package home

import java.util.*
import kotlin.collections.ArrayList

fun main(args: Array<String>) {
    var names: ArrayList<String?> = ArrayList()
    var people: ArrayList<String?> = ArrayList()

    names.add("Artem")
    names.add("Vasia")
    names.add("Petia")
    names.add("Dima")
    names.add(null)

    for (item: Int in 1..Random().nextInt(10)+10) {
        val temp: String? = randomName(names);
        people.add(temp)
    }
    println(people)
}

fun randomName(array: ArrayList<String?>): String? {
    return array.get(Random().nextInt(array.size))
}